# gitlab-pages-test
預設網址: https://gitlab-pages-test.gitlab.io/
建立新分支 dev 進行開發，再切回 main 分支合併觸發 GitLab Pages 自動部署。

## Getting started 開始

To make it easy for you to get started with GitLab, here's a list of recommended next steps.  
為了讓您輕鬆上手 GitLab，這裡列出了推薦的後續步驟。

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!  
已經是專業人士了嗎？只需編輯此 README.md 並使其成為您自己的。想讓它變得簡單嗎？使用底部的範本！

## Add your files 添加檔

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files  
創建或上傳檔
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:  
使用命令列新增檔案或使用以下命令推送現有 Git 儲存庫：

```
cd existing_repo
git remote add origin https://gitlab.com/cs-1688/gitlab-pages-test.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools 與您的工具整合

- [ ] [Set up project integrations](https://gitlab.com/cs-1688/gitlab-pages-test/-/settings/integrations)

## Collaborate with your team 與您的團隊協作

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)  
自動關閉合併請求中的問題
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)  
啟用合併請求審批
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy 測試和部署

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)  
GitLab CI/CD 入門
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)  
使用靜態應用程式安全測試 （SAST） 分析代碼中的已知漏洞
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)  
使用基於拉取的部署來改進 Kubernetes 管理
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README 編輯本自述檔

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.  
當您準備好製作自己的自述檔時，只需編輯此檔並使用下面方便的範本（或者隨意構建它 - 這隻是一個起點！感謝 makeareadme.com 提供此範本。

## Suggestions for a good README 對一個好的自述文件的建議

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.  
每個專案都是不同的，因此請考慮以下哪些部分適用於您的專案。範本中使用的部分是針對大多數開源項目的建議。還要記住，雖然 README 可能太長太詳細，但太長總比太短好。如果您認為您的自述檔太長，請考慮使用另一種形式的文檔，而不是刪除資訊。

## Name
Choose a self-explaining name for your project.  
為您的項目選擇一個不言自明的名稱。

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.  
讓人們知道你的項目具體可以做什麼。提供上下文並添加指向訪問者可能不熟悉的任何參考的連結。還可以在此處添加功能清單或背景小節。如果您的專案有替代方案，這是列出差異化因素的好地方。

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.  
在某些自述檔中，您可能會看到傳達元數據的小圖像，例如專案的所有測試是否都通過。您可以使用 Shields 將一些內容添加到您的自述檔中。許多服務還提供了添加徽章的說明。

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.  
根據您正在製作的內容，最好包含螢幕截圖甚至視頻（您經常會看到 GIF 而不是實際視頻）。像 ttygif 這樣的工具可以提供説明，但請查看 Asciinema 以獲得更複雜的方法。

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.  
在特定的生態系統中，可能有一種常見的安裝方式，例如使用 Yarn、NuGet 或 Homebrew。但是，請考慮以下可能性：正在閱讀您的自述檔的人是新手並希望獲得更多指導。列出具體步驟有助於消除歧義，並讓人們儘快使用您的專案。如果它僅在特定上下文（如特定程式設計語言版本或操作系統）中運行，或者具有必須手動安裝的依賴項，則還要添加“要求”小節。

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.  
大量使用範例，並盡可能顯示預期的輸出。內聯您可以演示的最小用法示例會很有幫助，同時如果它們太長而無法合理地包含在自述檔中，則提供指向更複雜示例的連結。

## Support 支援
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.  
告訴人們他們可以去哪裡尋求説明。它可以是問題跟蹤器、聊天室、電子郵件位址等的任意組合。

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.  
如果您對未來的發佈有想法，最好在自述檔中列出它們。

## Contributing
State if you are open to contributions and what your requirements are for accepting them.  
說明您是否願意接受貢獻以及您接受貢獻的要求是什麼。

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.  
對於想要對項目進行更改的人，有一些關於如何開始的文檔會很有説明。也許他們應該運行一個腳本，或者他們需要設置一些環境變數。明確這些步驟。這些說明也可能對你未來的自己有用。

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.  
您還可以記錄用於lint代碼或執行測試的命令。這些步驟有助於確保高代碼品質，並減少更改無意中破壞某些內容的可能性。如果需要外部設置，例如啟動 Selenium 伺服器以在瀏覽器中進行測試，則具有運行測試的說明特別有用。

## Authors and acknowledgment 作者和致謝
Show your appreciation to those who have contributed to the project.  
向那些為該專案做出貢獻的人表示感謝。

## License
For open source projects, say how it is licensed.  
對於開源專案，請說明它是如何獲得許可的。

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.  
如果你的項目已經耗盡了精力或時間，請在自述文件的頂部放一個註釋，說明開發已經放緩或完全停止。有人可能會選擇分叉你的專案，或者自願作為維護者或擁有者介入，讓你的專案繼續進行。您還可以向維護者提出明確的請求。
